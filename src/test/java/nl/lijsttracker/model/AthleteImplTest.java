package nl.lijsttracker.model;

import org.testng.Assert;
import org.testng.annotations.Test;

import nl.lijsttracker.model.LijsttrekkerImpl;


public class AthleteImplTest {

	@Test(groups = "unit")
	public void testNormalization() {
		Assert.assertEquals(new LijsttrekkerImpl("id02", "Kiki Bertens", "Bertens", "Tennis", "face:kiki", 231, 0, null).getNormalizedName(), "Kiki Bertens");
		Assert.assertEquals(new LijsttrekkerImpl("id02", "Noël van 't End", "End", "Tennis", "face:noel_end", 231, 1, null).getNormalizedName(), "Noel van t End");
		Assert.assertEquals(new LijsttrekkerImpl("id02", "Noël Ter-Aar", "Aar", "Tennis", "face:noel_teraar", 231, 2, null).getNormalizedName(), "Noel Ter Aar");
	}
	
}

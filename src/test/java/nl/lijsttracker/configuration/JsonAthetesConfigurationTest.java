package nl.lijsttracker.configuration;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.lijsttracker.configuration.LijsttrekkerConfig;
import nl.lijsttracker.configuration.ConfigurationManager;
import nl.lijsttracker.configuration.JsonLijsttrekkersConfiguration;

import static org.mockito.Mockito.*;

public class JsonAthetesConfigurationTest {

	@Test(groups = "unit")
	public void testDeserialization() {
		ConfigurationManager config = mock(ConfigurationManager.class);
		when(config.getProperty(JsonLijsttrekkersConfiguration.FILE_PROPERTY, JsonLijsttrekkersConfiguration.FILE_DEFAULT)).thenReturn("athletes.json");
		ObjectMapper mapper = new ObjectMapper();
		
		JsonLijsttrekkersConfiguration atheletesConfig = new JsonLijsttrekkersConfiguration(config, mapper);
		LijsttrekkerConfig athlete = atheletesConfig.get("angela_merkel");
		
		Assert.assertNotNull(athlete);
		Assert.assertEquals(athlete.getName(), "Angela Merkel");
		Assert.assertEquals(athlete.getParty(), "German Politics");
		Assert.assertEquals(athlete.getQuery(), "\"Angela Merkel\" OR face:\"angela_merkel\"");
	}
	
}

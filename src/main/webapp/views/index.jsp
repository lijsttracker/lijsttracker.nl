<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="Media Distillery">
		<meta name="description" content="LijstTracker.nl volgt in de aanloop naar de Tweede Kamerverkiezingen alle lijsttrekkers op de Nederlandse televisie. We analyseren alle beelden volledig automatisch op basis van gezichtsherkenning. Zo kunnen we exact meten hoe lang iedere lijsttrekker in beeld is geweest en hoeveel kiezers dit heeft bereikt.">
		<meta name="keywords" content="Tweede Kamer, Tweede Kamerverkiezingen, Verkiezingen, Verkiezingen 2017, Lijstrekkers, Lijsttrekkersdebat, Lijsttrekkers Debat, LijstTracker">
		<meta content="LijstTracker" property="og:site_name">
		<meta content="nl_NL" property="og:locale">
		
		<!-- facebook and linkedin share meta -->
		<meta property="og:url" content="http://www.lijsttracker.nl" />
		<meta property="og:title" content="Lijsttrekkers met de meeste 'face-time'"  />
		<meta property="og:description" content="Lijsttracker.nl geeft je een overzicht van de lijsttrekkers die het meest met hun gezicht op TV zijn geweest." />
		<meta property="og:image" content="http://www.lijsttracker.nl/img/og-image.jpg" />

		<!-- twitter card -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@MediaDistillery" />
		<meta name="twitter:title" content="Lijsttrekkers met de meeste 'face-time'" />
		<meta name="twitter:description" content="Lijsttracker.nl geeft je een overzicht van de lijsttrekkers die het meest met hun gezicht op TV zijn geweest." />
		<meta name="twitter:image" content="http://www.lijsttracker.nl/img/twitter-image.jpg" />
		
		<link type="image/x-icon" href="http://www.rioradar.nl/favicon.ico" rel="shortcut icon">
		<link href="img/radar.ico" rel="icon">
		
		<title>LijstTracker - Volg alle lijsttrekkers!</title>

		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
		<link rel="stylesheet" type="text/css" href="css/base.css">
		<link rel="stylesheet" type="text/css" href="css/nv.d3.css">
		
		<!-- [if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Google Analytics -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-61645655-5', 'auto');
		  ga('send', 'pageview');
		</script>
		
	</head>

	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="brand">
					<a href="/">LijstTracker.nl <span class="logo-font"></span></a>
				</div>
				<div class="flag">
					<div class="flag-red"></div>
					<div class="flag-blue"></div>
				</div>
			</div>
		</nav>
	
		<div class="plane-1">
			<div class="container">
				<div class="plane-header">
					<h2>Lijsttrekkers met de meeste "face-time"</h2>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Deze lijsttrekkers zijn het meest met hun gezicht op TV geweest / hebben meeste kijkers gehad:</p>
					</div>
				</div>
				<div id="popular-athletes-loading" class="row">
					<div class="col-xs-12">
						<p>De lijsttrekkers worden ingeladen</p>
					</div>
				</div>
				<div id="popular-athletes-row" class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="row">
							<!-- Names -->
							<div id="popular-athlete-names" class="sporter-names">
								<!-- Placeholder for names -->
							</div>
							<!-- Photos -->
							<div id="popular-athlete-tiles" class="sporter-tiles">
								<!-- Placeholder for photos -->
							</div>
							<!-- Bars -->
							<div id="popular-athlete-bars" class="sporter-bars">
								<!-- Placeholder for bars -->
							</div>
						</div>
					</div>
				</div>
				<div id="toggle-row" class="row">
					<div class="col-xs-12">
						<div class="toggle-info pull-left">Switch tussen "face-time" en de impact ervan</div>
						<div class="btn-group data-toggle" data-toggle="buttons">
							<label class="btn btn-default active">
								<input type="radio" name="impactOrDuration" value="duration" autocomplete="off" checked> "Face-time"
							</label>
							<label class="btn btn-default">
								<input type="radio" name="impactOrDuration" value="impact" autocomplete="off"> Bereik
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="plane-2">
			<div class="container">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="plane-2-text">
							<p>LijstTracker.nl heeft in aanloop naar de Tweede Kamerverkiezingen van 2017 alle lijsttrekkers op de Nederlandse televisie gevolgd. We hebben alle beelden volledig automatisch geanalyseerd op basis van gezichtsherkenning. Zo konden we exact meten hoe lang iedere lijsttrekker in beeld was en hoeveel kiezers dit had bereikt. Alle televisiebeelden tussen 1 januari en 15 maart 2017 zijn live geanalyseerd en de resultaten werden in realtime in beeld gebracht.</p>
						</div>
					</div>	
				</div>
			</div>
		</div>

		<div class="plane-3">
			<div class="container">
				<div class="plane-header">
					<h2>Kijkuren tussen 1 januari en 15 maart 2017</h2>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Deze grafiek laat het aantal kijkuren per lijsttrekker (Top 10) tussen 1 januari en 15 maart 2017 zien:</p>
					</div>
				</div>
				<div id="lijsttrekker-impact-row" class="row">
					<svg id="lijsttrekker-impact"></svg>
				</div>
				<div id="lijsttrekker-impact-loading" class="row">
					<div class="col-xs-12">
						<p>De lijsttrekkers worden ingeladen</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="plane-6">
			<div class="container">
				<div class="row">
					<div class="col-xs-10 col-xs-offset-1">
						<div class="plane-2-text">
							<p>LijstTracker.nl en Media Distillery verzamelen meer dan alleen de "face-time" van lijsttrekkers en de impact ervan. We weten bijvoorbeeld precies waar radio en TV programma's over gaan en wie erin te zien waren. Meer weten?</p>
							<a href="mailto:hello@mediadistillery.com?Subject=LijstTracker.nl" target="_top" role="button" class="btn btn-default">Neem contact op</a>
						</div>
					</div>	
				</div>
			</div>
		</div>

		<div class="plane-4">
			<div class="container">
				<div class="plane-header">
					<h2>Alle lijsttrekkers</h2>
				</div>
				<div id="browse-athletes-loading" class="row">
					<div class="col-xs-12 ">
						<p>De lijsttrekkers worden ingeladen</p>
					</div>
				</div>
				<div class="row browse-athletes-row">
					<div class="col-xs-10 col-xs-offset-1">
						<div id="sporter-tabs-nav" class="a-z-nav">
							<!-- Placeholder for navigation -->
						</div>
					</div>
				</div>
				<div class="row browse-athletes-row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div id="sporter-tabs" class="tab-content">
							<!-- Placeholder for tabs -->
						</div>
					</div>
				</div>
			</div>
		</div>	

		<div class="plane-5">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<span>LijstTracker.nl is een initiatief van:</span>
					</div>
				</div>
				<div class="row">
					<div class="logo-box col-sm-2">
						<a href="http://www.mediadistillery.com" target="_blank"><img src="img/mediadistillery.png" alt="Media Distillery"/></a>
					</div>
					<div class="logo-box col-sm-2">
						<a href="http://www.ipgmediabrands.com" target="_blank"><img src="img/mediabrands.png" alt="IPG Mediabrands"/></a>
					</div>
					<div class="logo-box col-sm-2">
						<a href="http://www.vicarvision.nl" target="_blank"><img src="img/vicarvision_sm.jpg" alt="VicarVision"/></a>
					</div>
				</div>
			</div>
		</div>
		
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js" charset="utf-8"></script>
		<script src="js/nv.d3.min.js"></script>
		
		<script src="js/lijsttracker.js"></script>
		
		<script>
			$( function() {
				loadAthletes();
			});
		</script>
		
	</body>

</html>
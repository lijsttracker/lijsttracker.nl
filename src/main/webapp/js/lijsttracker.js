/*
 * Loaded data
 */
var cached_lijsttrekkers = [];

function loadAthletes() {
	var athletesUrl = "api/1/lijsttrekkers";
	//var athletesUrl = "data/athletes.json";
	// Show loading
	$( "#browse-athletes-loading" ).show();
	$( ".browse-athletes-row" ).hide();
	$.ajax({
		type: 'GET',
		url: athletesUrl,
		dataType: "json",
		success: function( data ) {
			$( ".browse-athletes-row" ).show();
			onLijsttrekkersLoaded( data );
		}
	});
}

function onLijsttrekkersLoaded( lijsttrekkers ) {
	cached_lijsttrekkers = lijsttrekkers;
	$( "#popular-athletes-loading" ).hide();
	$( "#browse-athletes-loading" ).hide();
	$( "#lijsttrekker-impact-loading" ).hide();
	showFaceTimeBars();
	showAllAthletes( lijsttrekkers );
	$("input[type=radio][name=impactOrDuration]").change(function() {
		if (this.value == "impact") {
			showImpactBars();
		} else {
			showFaceTimeBars();
		}
	});
	showImpactGraph();
}

/*
 * Show the facetime bars
 */
function showFaceTimeBars() {
	lijsttrekkers = cached_lijsttrekkers.slice(0);
	var facetime = {
		comparator: compareByScore,
		getScore: function( lijsttrekker ) {
			return lijsttrekker.score;
		},
		getQuantity: getFaceTimeQuantity
	};
	showBars( lijsttrekkers, facetime );
}

function showImpactBars() {
	lijsttrekkers = cached_lijsttrekkers.slice(0);
	var impact = {
		comparator: compareByImpact,
		getScore: function( lijsttrekker ) {
			return lijsttrekker.impact;
		},
		getQuantity: getImpactQuantity
	};
	showBars( lijsttrekkers, impact );
}

function showBars( lijsttrekkers, view ) {
	// Clear
	$( "#popular-athlete-names" ).empty();
	$( "#popular-athlete-tiles" ).empty();
	$( "#popular-athlete-bars" ).empty();
	// Add new
	lijsttrekkers.sort( view.comparator );
	lijsttrekkers = lijsttrekkers.slice( 0, 6 );
	
	// TODO: Replace relativeWidth calculation by (score + lowerBoundary) / (max_score + lowerBoundary) * 100
	
	var max_score = 0;
	var min_score = Number.MAX_SAFE_INTEGER;
	for ( var i = 0; i < lijsttrekkers.length; i++ ) {
		var athlete = lijsttrekkers[i];
		max_score = Math.max( max_score, view.getScore( athlete ) );
		min_score = Math.min( min_score, view.getScore( athlete ) );
	}
	var lowerBoundary = ( max_score - min_score ) * 0.1;
	for ( var i = 0; i < lijsttrekkers.length; i++ ) {
		var lijsttrekker = lijsttrekkers[i];
		// Name
		$( "#popular-athlete-names" ).append(
			"<div class=\"sporter-name\">" +
				"<span>" + lijsttrekker.name + "</span>" +
			"</div>"		
		);
		// Photo
		$( "#popular-athlete-tiles" ).append(
			"<div class=\"sporter-tile-70\"><img src=\"./img/faces/" + lijsttrekker.id + ".jpg\"></div>"
		);
		// Bar
		var relativeWidth = Math.round( ( ( view.getScore( lijsttrekker ) - min_score + lowerBoundary ) / ( max_score - min_score + lowerBoundary) ) * 100 );
		var quantity = view.getQuantity( lijsttrekker );
		var bar = $( 
			"<div class=\"sporter-bar\">" +
				"<div class=\"duration\"><div class=\"value\">" + quantity.value + "</div></div>" + 
				"<div class=\"duration-unit\"><div class=\"value\">" + quantity.unit + "</div></div>" +
			"</div>"
		).data( "score", view.getScore( lijsttrekker ) ).width( "1%" ).animate({
			width: relativeWidth + "%"
		}, 2000, function() {
			$( ".duration", this ).show();
			$( ".duration-unit", this ).show();
		});
		$( ".duration", bar ).hide();
		$( ".duration-unit", bar ).hide();
		$( "#popular-athlete-bars" ).append( bar );
	}
}

function getFaceTimeQuantity( lijsttrekker ) {
	if ( lijsttrekker.score && lijsttrekker.score > 0 ) {
		//score = score * 15;
		var minutes = lijsttrekker.score / 60;
		var value = 0;
		var unit = "minuten";
		if ( minutes < 1 ) {
			value = lijsttrekker.score;
			unit = "seconden";
		} else {
			value = Math.round( lijsttrekker.score / 60 );
			if ( value == 1) {
				unit = "minuut";
			}
		}
		return { value : value, unit : unit };
	} else {
		return { value : 0, unit : "seconden" };
	}
}

function getImpactQuantity( lijsttrekker ) {
	var value = 0;
	var unit = "kijkuren";
	if ( lijsttrekker.impact && lijsttrekker.impact > 0 ) {
		var hours = lijsttrekker.impact / 60;
		if ( hours >= 1 ) {
			value = Math.round( lijsttrekker.impact / (60 * 1000) );
			return { value : addThousandsSeperator( value ) + "k", unit : unit };
		}
	}
	return { value : addThousandsSeperator( value ), unit : unit };
}


function addThousandsSeperator( value ) {
	return Intl.NumberFormat().format( value );
}

/*
 * Show the athlete tabs
 */
function showAllAthletes( athletes ) {
	var nav = $( "<span>" );
	var activatedTab = false;
	for ( var i = 0; i <= 25; i++ ) {
		var character = String.fromCharCode( i + 65 );
		var filteredAthletes = athleteNamesStartingWith( athletes, character );
		// Navigation
		if ( filteredAthletes.length == 0 ) {
			nav.append( "<span>" + character + "</span>" );
		} else {
			nav.append( "<a data-toggle=\"tab\" role=\"tab\" href=\"#aznav-" + character + "\">" + character + "</a>" );
		}
		if ( i < 25 ) {
			nav.append(" | " );
		}
		// Tab pane
		var tab = $( "<div id=\"aznav-" + character + "\" class=\"tab-pane\" role=\"tabpanel\"> </div>" );
		if ( !activatedTab && filteredAthletes.length > 0 ) {
			tab.addClass("active");
			activatedTab = true;
		}
		tab.append(
			"<div class=\"row\">" +
				"<div class=\"col-sm-12\">" +
					"<h3>" + character + "</h3>" +
				"</div>" +
			"</div>"
		);
		var tabPane = $( "<div class=\"row\">" );
		$.each( filteredAthletes.sort(compareBySurname), function( ix, athlete ) {
			var faceTimeQuantity = getFaceTimeQuantity( athlete );
			var impactQuantity = getImpactQuantity( athlete );
			var tile = $(
				"<div class=\"sporter-tile col-xs-12 col-sm-6 col-md-4\">" +
					"<div class=\"sporter-tile-70 pull-left\"><img src=\"./img/faces/"+athlete.id+".jpg\"></div>" +
					"<div class=\"sporter-name\">" +
						"<div class=\"name\">" + athlete.name + "</div>" +
						"<div class=\"sport\">" + athlete.party + "</div>" +
						"<div class=\"duration\">" + faceTimeQuantity.value + " " + faceTimeQuantity.unit + "</div>" +
						"<div class=\"duration\">" + impactQuantity.value + " " + impactQuantity.unit + "</div>" +
					"</div>" +
				"</div>"	
			);
			tabPane.append( tile );
		});
		tab.append( tabPane );
		$( "#sporter-tabs" ).append( tab );
	}
	$( "#sporter-tabs-nav" ).append( nav );
}


/*
 * Impact Graph
 */
function showImpactGraph() {
	lijsttrekkers = cached_lijsttrekkers.slice(0);
	lijsttrekkers.sort( compareByImpact );
	lijsttrekkers = lijsttrekkers.slice( 0, 10 );
	var data = getCumulativeImpact( lijsttrekkers );
	var colors = d3.scale.category20();
    var chart;
    nv.addGraph(function() {
        chart = nv.models.stackedAreaChart()
            .useInteractiveGuideline(true)
            .x(function(d) { return d[0] })
            .y(function(d) { return d[1] })
            .showControls(false)
            .showTotalInTooltip(false)
            .duration(300);
        chart.xAxis.tickFormat(function(d) { return d3.time.format('%d-%m')(new Date(d)) });
        chart.yAxis.tickFormat(function(d) {
        	var r = d3.format(',f')(d);
        	return r.replace(',','.') + "k";
        });
        chart.legend.vers('furious');
        chart.legend.margin({top: 5, right:0, left:0, bottom: 25});
        chart.interactiveLayer.tooltip.headerFormatter(function (d) {
        	return d3.time.format('%d-%m-%Y')(new Date(new Number(d)));
        });
        d3.select('#lijsttrekker-impact')
            .datum(data)
            .transition().duration(1000)
            .call(chart)
            .each('start', function() {
                setTimeout(function() {
                    d3.selectAll('#lijsttrekker-impact *').each(function() {
                        if(this.__transition__)
                            this.__transition__.duration = 1;
                    })
                }, 0)
            });
        nv.utils.windowResize(chart.update);
        return chart;
    });
	
}

function getCumulativeImpact( lijsttrekkers ) {
	var result = [];
	$.each( lijsttrekkers, function( ix, lijsttrekker ) {
		var cum_impact = [];
		var sum = 0;
		$.each( lijsttrekker.impactPerDay, function( timestamp, value ) {
			sum = sum + value;
			cum_impact.push( [ timestamp, sum / (60 * 1000) ] );
		});
		var entry = {
			key: lijsttrekker.name,
			values: cum_impact
		}
		result.push( entry );
	});
	return result;
}


/*
 * Util functions
 */
function athleteNamesStartingWith( athletes, start ) {
	var matcher = new RegExp("^" + start, "i" );
	return $.grep( athletes, function( athlete ) {
		return matcher.test( athlete.surname );
	});
}

function displayDate( epoch ) {
	var date = new Date( epoch );
	return date.toLocal('dd-MM-yyyy');
}

function displayRelativeTime(millis) {
	var LZ = function( x ) {
		return ( x<0 || x>9 ? "" : "0" ) + x;
	};
	var secs = millis / 1000;
	var result = LZ( Math.floor( secs % 60 ) );
	if ( secs >= 60 ) {
		result = LZ( Math.floor( secs / 60 ) % 60 ) + ":" + result;
	} else {
		result = "00:" + result;
	}
	if ( secs >= 3600 ) {
		result = Math.floor( secs / 3600 ) + ":" + result;
	}
	return result;
}

function compareByScore( one, other ) {
	if ( one.score < other.score )
		return 1;
	if ( one.score > other.score )
		return -1;
	return 0;
}

function compareByImpact( one, other ) {
	if ( one.impact < other.impact )
		return 1;
	if ( one.impact > other.impact )
		return -1;
	return 0;
}

function compareBySurname( one, other ) {
	if ( one.surname < other.surname )
		return -1;
	if ( one.surname > other.surname )
		return 1;
	return 0;
}

package nl.lijsttracker.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.LijsttrekkerConfig;
import nl.lijsttracker.configuration.LijsttrekkersConfiguration;
import nl.lijsttracker.model.Lijsttrekker;
import nl.lijsttracker.model.LijsttrekkerDescendingScoreComparator;
import nl.lijsttracker.model.LijsttrekkerNameComparator;
import nl.lijsttracker.stats.StatsCollector;

@Service
@Singleton
public class CachedLijsttrekkerService implements LijsttrekkerService {

	private final Logger LOG = Logger.getLogger(CachedLijsttrekkerService.class);
	
	//Keeps all Athletes sorted in the map.
	private final AtomicReference<Map<String,Lijsttrekker>> lijsttrekkers = new AtomicReference<Map<String,Lijsttrekker>>();
	private final LijsttrekkersConfiguration lijsttrekkersConfig;
	private final StatsCollector lijsttrekkersStatsCollector;
	private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	private final CountDownLatch refreshLatch = new CountDownLatch(1);
	
	@Inject
	public CachedLijsttrekkerService(LijsttrekkersConfiguration lijsttrekkersConfig, StatsCollector collector) {
		this.lijsttrekkersConfig =  lijsttrekkersConfig;
		this.lijsttrekkersStatsCollector = collector;
		executor.scheduleAtFixedRate(new RefreshRunner(), 0, 15, TimeUnit.MINUTES);
	}
	
	@Override
	public List<Lijsttrekker> getTopN(int count) {
		if (requiresRefresh()) {
			refreshLijsttrekkerStats();
		}
		List<Lijsttrekker> result = new ArrayList<Lijsttrekker>(count);
		try {
			refreshLatch.await(60, TimeUnit.SECONDS);
			Iterator<Lijsttrekker> iter = lijsttrekkers.get().values().iterator();
			for (int i=0; i<count; i++) {
				if(iter.hasNext()) {
					result.add(iter.next());
				} else {
					break;
				}
			}
			return result;
		} catch (InterruptedException e) {
			throw new RuntimeException("Loading the lijsttrekker data took too long", e);
		}
	}

	@Override
	public Lijsttrekker getLijsttrekker(String id) {
		if (requiresRefresh()) {
			refreshLijsttrekkerStats();
		}
		try {
			refreshLatch.await(60, TimeUnit.SECONDS);
			return lijsttrekkers.get().get(id);
		} catch (InterruptedException e) {
			throw new RuntimeException("Loading the lijsttrekker data took too long", e);
		}
	}

	private void refreshLijsttrekkerStats() {
		LOG.info("Reloading statistics for all lijsttrekkers...");
		List<Lijsttrekker> allLijsttrekkers = new LinkedList<Lijsttrekker>();
		List<LijsttrekkerConfig> configuredLijsttrekkers = lijsttrekkersConfig.getAll();
		for (LijsttrekkerConfig config : configuredLijsttrekkers) {
			Lijsttrekker lijsttrekker = null;
			try {
				lijsttrekker = lijsttrekkersStatsCollector.getLijsttrekkerScore(config);
				allLijsttrekkers.add(lijsttrekker);
				LOG.info("Lijsttrekker "+lijsttrekker.getName()+": "+lijsttrekker.getScore());
			} catch (Exception e) {
				LOG.error("Unable to load data for lijsttrekker "+lijsttrekker.getName(), e);
			}
		}
		Collections.sort(allLijsttrekkers, new LijsttrekkerDescendingScoreComparator());
		Map<String,Lijsttrekker> map = new LinkedHashMap<String,Lijsttrekker>();
		for (Lijsttrekker a : allLijsttrekkers) {
			map.put(a.getId(), a);
			LOG.info("Name: "+a.getName()+" score: "+a.getScore());
		}
		lijsttrekkers.set(map);
		refreshLatch.countDown();
		LOG.info("Pulbished new statistic for all lijsttrekkers.");
	}


	private boolean requiresRefresh() {
		return lijsttrekkers.get() == null;
	}

	private class RefreshRunner implements Runnable {

		@Override
		public void run() {
			try {
				refreshLijsttrekkerStats();
			} catch(Exception e) {
				LOG.error("Unhandled exception while reloading data for lijsttrekkers.");
			}
		}
		
	}

	@Override
	public List<Lijsttrekker> getAll() {
		if (requiresRefresh()) {
			refreshLijsttrekkerStats();
		}
		try {
			refreshLatch.await(60, TimeUnit.SECONDS);
			List<Lijsttrekker> all = new LinkedList<Lijsttrekker>(lijsttrekkers.get().values());
			Collections.sort(all, new LijsttrekkerNameComparator());
			return all;
		} catch (InterruptedException e) {
			throw new RuntimeException("Loading the lijsttrekker data took too long", e);
		}
	}
	
}

	
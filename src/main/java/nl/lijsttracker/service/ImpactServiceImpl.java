package nl.lijsttracker.service;

import java.util.Calendar;
import java.util.Map.Entry;
import java.util.SortedMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.LijsttrekkersConfiguration;
import nl.lijsttracker.impact.ImpactDAO;

@Service
@Singleton
public class ImpactServiceImpl implements ImpactService {

	private final Logger LOG = Logger.getLogger(ImpactServiceImpl.class);
	
	private final LijsttrekkersConfiguration lijsttrekkersConfig;
	private final ImpactDAO impactDAO;
	
	@Inject
	public ImpactServiceImpl(LijsttrekkersConfiguration lijsttrekkersConfig, ImpactDAO impactDAO) {
		this.lijsttrekkersConfig = lijsttrekkersConfig;
		this.impactDAO = impactDAO;
	}

	@Override
	public int getImpactSum(String lijsttrekkerId, Calendar from, Calendar to) {
		int sum = 0;
		for (Entry<Long, Integer> entry: impactDAO.getImpact(lijsttrekkerId, from, to).entrySet()) {
			sum += entry.getValue();
		}
		return sum;
	}
	
	@Override
	public SortedMap<Long, Integer> getImpact(String lijsttrekkerId, Calendar from, Calendar to) {
		return impactDAO.getImpact(lijsttrekkerId, from, to);
	}

	@Override
	public void storeImpact(String lijsttrekkerId, Calendar date, int impact) {
		if (impact < 0) { 
			throw new IllegalArgumentException("The provided impact (" + impact + ") is not within the range [0,2147483647]");
		} else if (lijsttrekkersConfig.get(lijsttrekkerId) == null) { 
			throw new IllegalArgumentException("The lijsttrekker with Id '" + lijsttrekkerId + "' does not exist");
		} else {
			LOG.info("Storing impact data (" + impact + ") for lijsttrekker with Id '" + lijsttrekkerId + "'"); 
			impactDAO.storeImpact(lijsttrekkerId, date, impact);
		}
	}

}

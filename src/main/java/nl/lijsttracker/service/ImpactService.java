package nl.lijsttracker.service;

import java.util.Calendar;
import java.util.SortedMap;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface ImpactService {

	public int getImpactSum(String lijsttrekkerId, Calendar from, Calendar to);
	
	public SortedMap<Long, Integer> getImpact(String lijsttrekkerId, Calendar from, Calendar to);
	
	public void storeImpact(String lijsttrekkerId, Calendar date, int impact);
	
}

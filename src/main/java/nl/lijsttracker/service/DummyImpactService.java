package nl.lijsttracker.service;

import java.util.Calendar;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Singleton;

import org.jvnet.hk2.annotations.Service;

@Service
@Singleton
public class DummyImpactService implements ImpactService {

	@Override
	public int getImpactSum(String lijsttrekkerId, Calendar from, Calendar to) {
		return 10;
	}

	@Override
	public void storeImpact(String lijsttrekkerId, Calendar date, int impact) {	}

	@Override
	public SortedMap<Long, Integer> getImpact(String lijsttrekkerId, Calendar from, Calendar to) {
		return new TreeMap<>();
	}

}

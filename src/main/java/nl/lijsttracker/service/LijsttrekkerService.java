package nl.lijsttracker.service;

import java.util.List;

import org.jvnet.hk2.annotations.Contract;

import nl.lijsttracker.model.Lijsttrekker;

@Contract
public interface LijsttrekkerService {

	public List<Lijsttrekker> getTopN(int count);

	public Lijsttrekker getLijsttrekker(String id);

	public List<Lijsttrekker> getAll();
	
}

package nl.lijsttracker.stats;

import org.jvnet.hk2.annotations.Contract;

import nl.lijsttracker.configuration.LijsttrekkerConfig;
import nl.lijsttracker.model.Lijsttrekker;

@Contract
public interface StatsCollector {

	Lijsttrekker getLijsttrekkerScore(LijsttrekkerConfig config);
	
}

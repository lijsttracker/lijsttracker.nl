package nl.lijsttracker.stats;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.SortedMap;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.LijsttrekkerConfig;
import nl.lijsttracker.configuration.ConfigurationManager;
import nl.lijsttracker.model.Lijsttrekker;
import nl.lijsttracker.model.LijsttrekkerImpl;
import nl.lijsttracker.service.ImpactService;
import tv.mediadistillery.api.client.searchservice.SearchResult;
import tv.mediadistillery.api.client.searchservice.SearchResultProgram;
import tv.mediadistillery.api.client.searchservice.SearchResultSnippet;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;
import tv.mediadistillery.api.client.searchservice.params.Sort;

@Service
@Singleton
public class StatsCollectorImpl implements StatsCollector {

	private final SearchServiceClient searchClient;
	private final ImpactService impactService;
	private final int speechScore;
	private final int faceScore;
	private final int defaultScore;
	
	@Inject
	public StatsCollectorImpl(SearchServiceClient searchClient, ImpactService impactService, ConfigurationManager config) {
		this.searchClient = searchClient;
		this.impactService = impactService;
		this.speechScore = config.getIntProperty("lijsttrekkers.score.speech", 15);
		this.faceScore = config.getIntProperty("lijsttrekkers.score.face", 2);
		this.defaultScore = config.getIntProperty("lijsttrekkers.score.other", 1);
	}
	
	@Override
	public Lijsttrekker getLijsttrekkerScore(LijsttrekkerConfig config) {
		// Face-time
		int totalScore = 0;
		SearchResult result = searchClient.search(config.getQuery(), 0, 10000, Sort.START_DESC, "start > \"01-01-2017\" AND start < \"15-03-2017\"");
		if (result != null && result.getPrograms() != null) {
			for (SearchResultProgram program : result.getPrograms()) {
				for (SearchResultSnippet snippet : program.getSnippets()) {
					totalScore += scoreMentionsIn(snippet);
				}
			}
		}
		// Impact
		Calendar from = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		from.set(2017, 0, 1, 0, 0, 0);
		Calendar to = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		to.set(2017, 2, 15, 0, 0, 0);
		int impact = impactService.getImpactSum(config.getId(), from, to);
		SortedMap<Long, Integer> impactPerDay = impactService.getImpact(config.getId(), from, to);
		Lijsttrekker athlete = new LijsttrekkerImpl(config.getId(), config.getName(), config.getSurname(), config.getParty(), config.getQuery(), totalScore, impact, impactPerDay);
		return athlete;
	}

	private int scoreMentionsIn(SearchResultSnippet snippet) {
		String text = snippet.getSnippet();
		String source = snippet.getSource();
		int numOfMentions = text.split("<em>").length;
		if (source.equalsIgnoreCase("annotations.md::speech")) {
			return speechScore * numOfMentions;
		} else if (source.equalsIgnoreCase("annotations.md::face")) {
			return faceScore * numOfMentions;
		} else {
			return defaultScore * numOfMentions;
		}
	}

}

package nl.lijsttracker.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.jvnet.hk2.annotations.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;

@Service
@Singleton
public class JsonLijsttrekkersConfiguration implements LijsttrekkersConfiguration {

	static final String FILE_DEFAULT = "./src/main/resources/lijsttrekkers.json";
	static final String FILE_PROPERTY = "lijsttrekkers.json";
	private final Logger LOG = Logger.getLogger(JsonLijsttrekkersConfiguration.class);
	private final List<LijsttrekkerConfig> lijsttrekkers = new LinkedList<>();
	private final Map<String,LijsttrekkerConfig> index = new HashMap<>();
	
	@Inject
	public JsonLijsttrekkersConfiguration(ConfigurationManager config, ObjectMapper mapper) {
		String filename = config.getProperty(FILE_PROPERTY, FILE_DEFAULT);
		try {
			InputStream in = JsonLijsttrekkersConfiguration.class.getClassLoader().getResourceAsStream(filename);
			InputStreamReader reader = new InputStreamReader(in, Charsets.UTF_8);
			List<LijsttrekkerConfig> lijsttrekkers = mapper.readValue(reader, new TypeReference<LinkedList<LijsttrekkerConfigImpl>>(){});
			in.close();
			this.lijsttrekkers.addAll(lijsttrekkers);
			for (LijsttrekkerConfig c : lijsttrekkers) {
				index.put(c.getId(), c);
			}
		} catch (IOException e) {
			LOG.error("Unable to load the list of lijsttrekkers from "+filename, e);
		}
	}
	
	@Override
	public List<LijsttrekkerConfig> getAll() {
		return lijsttrekkers;
	}

	@Override
	public LijsttrekkerConfig get(String id) {
		if (index.containsKey(id)) { 
			return index.get(id);
		} else {
			return null;
		}
	}

	
	
}


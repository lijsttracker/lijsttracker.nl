package nl.lijsttracker.configuration;


public interface LijsttrekkerConfig {

	public String getId();
	
	public String getName();
	
	public String getSurname();

	public String getParty();
	
	public String getQuery();

}

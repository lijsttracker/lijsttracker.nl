package nl.lijsttracker.configuration;

import java.util.List;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface LijsttrekkersConfiguration {

	List<LijsttrekkerConfig> getAll();
	
	LijsttrekkerConfig get(String id);
	
}

package nl.lijsttracker.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class LijsttrekkerConfigImpl implements LijsttrekkerConfig {

	private final String id;
	private final String fullname;
	private final String surname;
	private final String query; 
	private final String party;
	
	@JsonCreator
	public LijsttrekkerConfigImpl(@JsonProperty("id") String id, @JsonProperty("name") String fullname, @JsonProperty("surname") String surname, @JsonProperty("sport") String party, @JsonProperty("query") String query) {
		super();
		this.id = id;
		this.fullname = fullname;
		this.surname = surname;
		this.party = party;
		this.query = query;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return fullname;
	}

	@Override
	public String getSurname() {
		return surname;
	}
	
	@Override
	public String getQuery() {
		return query;
	}

	@Override
	public String getParty() {
		return party;
	}	

}

package nl.lijsttracker.model;

import java.util.Comparator;

public class LijsttrekkerNameComparator implements Comparator<Lijsttrekker> {

	@Override
	public int compare(Lijsttrekker one, Lijsttrekker other) {
		return one.getName().compareTo(other.getName());
	}

}

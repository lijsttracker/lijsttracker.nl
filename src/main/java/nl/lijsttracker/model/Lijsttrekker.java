package nl.lijsttracker.model;

import java.util.SortedMap;

public interface Lijsttrekker {

	public String getId();
	
	public String getName();
	
	public String getSurname();

	public String getNormalizedName();
	
	public int getScore();
	
	public int getImpact();
	
	public SortedMap<Long, Integer> getImpactPerDay();
	
	public String getQuery();

	public String getParty();
	
}

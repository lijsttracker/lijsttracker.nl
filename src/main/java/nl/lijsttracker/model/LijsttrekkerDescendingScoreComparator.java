package nl.lijsttracker.model;

import java.util.Comparator;

public class LijsttrekkerDescendingScoreComparator implements Comparator<Lijsttrekker> {

	@Override
	public int compare(Lijsttrekker one, Lijsttrekker other) {
		Integer score1 = one.getScore();
		Integer score2 = other.getScore();
		return score2.compareTo(score1);
	}

}

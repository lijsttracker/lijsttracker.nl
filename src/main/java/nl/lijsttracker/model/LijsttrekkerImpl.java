package nl.lijsttracker.model;

import java.util.SortedMap;

import org.apache.commons.lang3.StringUtils;

public class LijsttrekkerImpl implements Lijsttrekker {

	private final String id;
	private final String fullname;
	private final String surname;
	private final int score;
	private final int impact;
	private final SortedMap<Long, Integer> impactPerDay;
	private final String sport;
	private final String query;
	
	public LijsttrekkerImpl(String id, String fullname, String surname, String party, String query, int score, int impact, SortedMap<Long, Integer> impactPerDay) {
		this.id = id;
		this.fullname = fullname;
		this.surname = surname;
		this.sport = party;
		this.score = score;
		this.query = query;
		this.impact = impact;
		this.impactPerDay = impactPerDay;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return fullname;
	}
	
	@Override
	public String getSurname() {
		return surname;
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public String getParty() {
		return sport;
	}

	@Override
	public String getNormalizedName() {
		return StringUtils.stripAccents(fullname).replace("'", "").replace("’", "").replace("-", " ");
	}

	@Override
	public int getImpact() {
		return impact;
	}
	
	@Override
	public SortedMap<Long, Integer> getImpactPerDay() {
		return impactPerDay;
	}

	@Override
	public String getQuery() {
		return query;
	}
	
}

package nl.lijsttracker.rest;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.api.Immediate;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.service.ImpactService;

@Immediate
@Path("/api/1/")
@Singleton @Service
public class ImpactResource {

	private final ImpactService impactService;
	
	@Inject
	public ImpactResource(ImpactService impactService) {
		this.impactService = impactService;
	}

	@POST
	@Secured
	@Path("/impact/{lijsttrekker}/{year}/{month}/{day}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response pushImpact(@PathParam("lijsttrekker") String lijsttrekkerId, 
			@PathParam("year") Integer year, @PathParam("month") Integer month, @PathParam("day") Integer day,
			Integer impact) {
		Calendar date = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		date.set(year, month - 1, day, 0, 0, 0);
		impactService.storeImpact(lijsttrekkerId, date, impact);
		return Response.noContent().build();
	}
	
	@GET
	@Path("/impact/{lijsttrekker}")
	@NoCaching
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImpactByLijsttrekker(@PathParam("lijsttrekker") String lijsttrekkerId,
			@QueryParam("from") @DefaultValue("0") Long from, 
			@QueryParam("to") @DefaultValue("-1") Long to) {
		Calendar fromCal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		fromCal.setTime(new Date(from));
		Calendar toCal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		if (to == -1) {
			toCal.setTime(new Date());
			toCal.add(Calendar.DATE, 1);
		} else {
			toCal.setTime(new Date(to));
		}
		Map<Long, Integer> impact = impactService.getImpact(lijsttrekkerId, fromCal, toCal);
		return Response.ok(impact).build();
	}
	
}

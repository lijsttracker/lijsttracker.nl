package nl.lijsttracker.rest;

import java.util.HashMap;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.server.mvc.Viewable;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.model.Lijsttrekker;
import nl.lijsttracker.service.LijsttrekkerService;

@Path("/")
@Singleton @Service
public class HomeResource {

	private final LijsttrekkerService lijsttrekkerService;
	
	@Inject
	public HomeResource(LijsttrekkerService lijsttrekkerService) {
		this.lijsttrekkerService = lijsttrekkerService;
	}
	
	@GET
	@PermitAll
	@Produces(MediaType.TEXT_HTML)
	public Response getView() {
		HashMap<String, String> variables = new HashMap<>();
		return Response.ok(new Viewable("/index.jsp", variables)).build();
	}
	
	@GET
	@PermitAll
	@Path("lijsttrekkers/{lijsttrekker}")
	@Produces(MediaType.TEXT_HTML)
	public Response getAthleteView(@PathParam("lijsttrekker") String lijsttrekkerId) {
		Lijsttrekker lijsttrekker = lijsttrekkerService.getLijsttrekker(lijsttrekkerId);
		HashMap<String, Object> variables = new HashMap<>();
		variables.put("lijsttrekker", lijsttrekker);
		variables.put("path", "../");
		return Response.status(Status.NOT_FOUND).entity(new Viewable("/no_longer_found.jsp", variables)).build();
	}
	
}

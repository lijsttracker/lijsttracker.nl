package nl.lijsttracker.rest;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import nl.lijsttracker.configuration.ConfigurationManager;

@Secured
@Provider
public class SecuredRequestFilter implements ContainerRequestFilter {

	private static final Response ACCESS_DENIED = Response.status(Response.Status.UNAUTHORIZED)
            .entity("Acces Denied. Please provide the correct username and password").build();
	
	private final String username;
	private final String password;
	
	@Inject
	public SecuredRequestFilter(ConfigurationManager config) {
		this.username = config.getProperty("impact.endpoint.username");
		this.password = config.getProperty("impact.endpoint.password");
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		// Get request headers
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();
        final List<String> authorization = headers.get("Authorization");
        if (authorization == null || authorization.isEmpty()) {
            requestContext.abortWith(ACCESS_DENIED);
            return;
        }
        final String encodedUserPassword = authorization.get(0).replaceFirst("Basic ", "");
        String usernameAndPassword = new String(Base64.decode(encodedUserPassword.getBytes()));;
        final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();
		if (!(username.equals(this.username) && password.equals(this.password))) {
			requestContext.abortWith(ACCESS_DENIED);
            return;
		}
	}

}

package nl.lijsttracker.rest;

public class ErrorMessage {

	private String cause;
	private String message;

	public ErrorMessage(String cause, String message) {
		this.cause = cause;
		this.message = message;
	}	

	public String getCause() {
		return cause;
	}
	
	public String getMessage() {
		return message;
	}
		
	public String getStatus() {
		return "error";
	}

}

package nl.lijsttracker.rest;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.api.Immediate;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.model.Lijsttrekker;
import nl.lijsttracker.service.LijsttrekkerService;

@Immediate
@Path("/api/1/")
@Singleton @Service
public class LijsttrekkerResource {

	private final LijsttrekkerService lijsttrekkerService;
	
	@Inject
	public LijsttrekkerResource(LijsttrekkerService lijsttrekkerService) {
		this.lijsttrekkerService = lijsttrekkerService;
	}

	@GET
	@PermitAll
	@Path("/lijsttrekkers/top")
	@NoCaching
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopN(@QueryParam("size") Integer size) {
		if (size == null) {
			size = 8;
		}
		List<Lijsttrekker> lijsttrekkers = lijsttrekkerService.getTopN(size);
		return Response.ok(lijsttrekkers).build();
	}
	
	@GET
	@PermitAll
	@Path("/lijsttrekkers")
	@NoCaching
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLijsttrekkers() {
		List<Lijsttrekker> lijsttrekkers = lijsttrekkerService.getAll();
		return Response.ok(lijsttrekkers).build();
	}

}

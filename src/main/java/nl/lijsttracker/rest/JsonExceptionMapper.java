package nl.lijsttracker.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

@Provider
public class JsonExceptionMapper implements ExceptionMapper<Throwable> {

	private static final Logger LOG = Logger.getLogger(JsonExceptionMapper.class);
	
	@Context 
	private HttpServletRequest request;
	
	@Override
	public Response toResponse(Throwable e) {
		LOG.error("Exception caught while handeling request.", e);
		ErrorMessage em = new ErrorMessage(
				(e.getCause() == null ? "" : e.getCause().toString()), 
				e.getMessage());
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(em).build();
	}

}

package nl.lijsttracker.impact;

import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.serializers.LongSerializer;
import com.netflix.astyanax.serializers.StringSerializer;

public class ColumnFamilies {

	public static final ColumnFamily<String, Long> CF_IMPACT = new ColumnFamily<String, Long>(
			"Impact",
			StringSerializer.get(),
			LongSerializer.get(),
			StringSerializer.get());

}

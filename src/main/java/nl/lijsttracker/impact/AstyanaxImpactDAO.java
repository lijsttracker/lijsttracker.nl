package nl.lijsttracker.impact;

import java.util.Calendar;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Inject;

import org.jvnet.hk2.annotations.Service;

import com.netflix.astyanax.ColumnMutation;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnList;

@Service
public class AstyanaxImpactDAO implements ImpactDAO {
	
	private final Keyspace keyspace;
	
	@Inject
	public AstyanaxImpactDAO(final Keyspace keyspace) {
		this.keyspace = keyspace;
	}

	@Override
	public SortedMap<Long, Integer> getImpact(String lijsttrekkerId, Calendar from, Calendar to) throws DAOException {
		try {
			Calendar f = createClone(from, 11, 59, 59, 0);
			Calendar t = createClone(to, 12, 0, 1, 0);
			TreeMap<Long, Integer> result = new TreeMap<>();
			OperationResult<ColumnList<Long>> queryResult = keyspace.prepareQuery(ColumnFamilies.CF_IMPACT).getRow(lijsttrekkerId)
				.withColumnRange(f.getTimeInMillis(), t.getTimeInMillis(), false, Integer.MAX_VALUE).execute();
			Iterator<Column<Long>> iterator = queryResult.getResult().iterator();
			while(iterator.hasNext()) {
				Column<Long> column = iterator.next();
				int value = column.getIntegerValue();
				result.put(column.getName(), value);
			}
			return result;
		} catch (ConnectionException e) {
			throw new DAOException("Unable to get the impact data for " + lijsttrekkerId, e);
		}
	}

	@Override
	public void storeImpact(String lijsttrekkerId, Calendar date, int impact) throws DAOException {
		try {
			Calendar d = createClone(date, 12, 0, 0, 0);
			ColumnMutation cm = keyspace.prepareColumnMutation(ColumnFamilies.CF_IMPACT, lijsttrekkerId, d.getTimeInMillis());
			cm.putValue(impact, null).execute();
		} catch (ConnectionException e) {
			throw new DAOException("Unable to store the impact data for " + lijsttrekkerId, e);
		}
	}
	
	private Calendar createClone(Calendar cal, int hours, int minutes, int seconds, int millis) {
		Calendar c = (Calendar) cal.clone();
		c.set(Calendar.HOUR_OF_DAY, hours);
		c.set(Calendar.MINUTE, minutes);
		c.set(Calendar.SECOND, seconds);
		c.set(Calendar.MILLISECOND, millis);
		return c;
	}

}

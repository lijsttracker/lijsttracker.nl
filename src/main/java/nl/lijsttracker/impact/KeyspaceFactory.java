package nl.lijsttracker.impact;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.api.Immediate;
import org.jvnet.hk2.annotations.Service;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.ddl.KeyspaceDefinition;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;

import nl.lijsttracker.configuration.ConfigurationManager;

@Immediate
@Service
public class KeyspaceFactory implements Factory<Keyspace> {

    private final ConfigurationManager      config;
    private final AstyanaxContext<Keyspace> context;
    
    @Inject
	public KeyspaceFactory(ConfigurationManager config) {
		this.config = config;
		// Read configuration
        final String clusterName = config.getProperty("lijsttracker.datastore.cluster", "LocalCluster");
        final String keyspace = config.getProperty("lijsttracker.datastore.keyspace", "LijstTracker");
        final String hosts = config.getProperty("lijsttracker.datastore.hosts");
        final int port = config.getIntProperty("lijsttracker.datastore.port", 9160);
        final int maxConnections = config.getIntProperty("lijsttracker.datastore.maxconnections", 1);
        // Create AstyanaxContext
        context = new AstyanaxContext.Builder().forCluster(clusterName).forKeyspace(keyspace)
        		.withAstyanaxConfiguration(new AstyanaxConfigurationImpl()
        				.setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE))
        		.withConnectionPoolConfiguration(
        				new ConnectionPoolConfigurationImpl(clusterName + "ConnectionPool")
        				.setPort(port).setMaxConnsPerHost(maxConnections)
        				.setSeeds(hosts))
        		.withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
        		.buildKeyspace(ThriftFamilyFactory.getInstance());
	}

    @PostConstruct
    public void postConstruct() {
        context.start();
        Keyspace keyspace = context.getClient();
        try {
            final KeyspaceDefinition def = keyspace.describeKeyspace();
            createColumnFamilies(def, keyspace);
        } catch (final ConnectionException e) {
            try {
                createKeyspace(keyspace);
            } catch (final ConnectionException ce) {
                throw new RuntimeException("Unable to create keyspace", ce);
            }
            try {
                createColumnFamilies(null, keyspace);
            } catch (final ConnectionException ce) {
                throw new RuntimeException("Unable to create one of the column families", ce);
            }
            /*
            TODO: Load default data
            try {
                addDefaultData(keyspace);
            } catch (final Exception ce) {
                throw new RuntimeException("Unable to add the default data to the keyspace", ce);
            }
            */
        }
        keyspace = null;
    }
    
    
    private void createKeyspace(final Keyspace keyspace) throws ConnectionException {
        keyspace.createKeyspace(ImmutableMap.<String, Object> builder()
        		.put("strategy_options",
        				ImmutableMap.<String, Object> builder().put("replication_factor",
        						config.getProperty("lijsttracker.datastore.replication_factor", "2"))
        				.build())
        		.put("strategy_class",
        				config.getProperty("lijsttracker.datastore.strategy_class", "SimpleStrategy"))
        		.build());
    }
    
    private void createColumnFamilies(final KeyspaceDefinition def, final Keyspace keyspace) throws ConnectionException {
    	if (def == null || def.getColumnFamily(ColumnFamilies.CF_IMPACT.getName()) == null) {
	    	final ImmutableMap<String, Object> impactCfOptions = ImmutableMap.<String, Object> builder()
	                .put("key_validation_class", "UTF8Type")
	                .put("comparator_type", "LongType")
	                .put("default_validation_class", "IntegerType").build();
	    	keyspace.createColumnFamily(ColumnFamilies.CF_IMPACT, impactCfOptions);
    	}
    }
    
	@Override
	public void dispose(Keyspace keyspace) { }

	@Override
	public Keyspace provide() {
		return context.getClient();
	}

}

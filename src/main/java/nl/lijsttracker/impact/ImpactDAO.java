package nl.lijsttracker.impact;

import java.util.Calendar;
import java.util.SortedMap;

import org.jvnet.hk2.annotations.Contract;

@Contract
public interface ImpactDAO {

	public SortedMap<Long, Integer> getImpact(String lijsttrekkerId, Calendar from, Calendar to) throws DAOException;
	
	public void storeImpact(String lijsttrekkerId, Calendar date, int impact) throws DAOException;
	
}

package nl.lijsttracker.search;

import java.util.List;

import tv.mediadistillery.api.client.searchservice.filter.FilterBuilder;

public interface FilterFactory {

	public FilterBuilder createFilter(FilterCondition condition);
	
	public FilterBuilder createFilter(List<FilterCondition> conditions);
	
}

package nl.lijsttracker.search;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.inject.Inject;

import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.ConfigurationManager;
import tv.mediadistillery.api.client.searchservice.filter.DateTimeThreshold;
import tv.mediadistillery.api.client.searchservice.filter.EmptyFilterBuilder;
import tv.mediadistillery.api.client.searchservice.filter.FilterBuilder;
import tv.mediadistillery.api.client.searchservice.filter.FilterBuilders;
import tv.mediadistillery.api.client.searchservice.filter.FixedDateTimeThreshold;
import tv.mediadistillery.api.client.searchservice.filter.RelativeDateTimeThreshold;

@Service
public class FilterFactoryImpl implements FilterFactory {
	
	private final SimpleDateFormat dateFormat;
	private final FilterBuilder dateRangeFilter;
	
	@Inject
	public FilterFactoryImpl(ConfigurationManager config) {
		this.dateFormat = new SimpleDateFormat(config.getProperty("bnr.search.date-filter-format", "dd-MM-yyyy"));
		int monthsHistory = config.getIntProperty("bnr.search.history", 3) * -1;
		this.dateRangeFilter = FilterBuilders.dateTimeFrom(new RelativeDateTimeThreshold(Calendar.MONTH, monthsHistory, 0, 0, 0, 0), true);
	}
	
	@Override
	public FilterBuilder createFilter(FilterCondition condition) {
		String field = condition.getField();
		TimeZone timezone = TimeZone.getTimeZone("Europe/Amsterdam");
		if (field.equalsIgnoreCase("searchPeriod")) {
			if (condition.getValue().equalsIgnoreCase("lastWeek")) {
				return FilterBuilders.dateTimeFrom(DateTimeThreshold.LAST_WEEK, true).timezone(timezone);
			} else if (condition.getValue().equalsIgnoreCase("lastTwoWeeks")) {
				return FilterBuilders.dateTimeFrom(new RelativeDateTimeThreshold(Calendar.DATE, -14, 0, 0, 0, 0), true).timezone(timezone);
			} else {
				return null;
			}
		} else if (field.equalsIgnoreCase("searchFrom")) {
			long from = parseDateTime(condition.getValue());
			if (from >= 0) {
				return FilterBuilders.dateTimeFrom(new FixedDateTimeThreshold(from), true);
			} else {
				return null;
			}
		} else if (field.equalsIgnoreCase("searchTill")) {
			long to = parseDateTime(condition.getValue());
			if (to >= 0) {
				return FilterBuilders.dateTimeTo(new FixedDateTimeThreshold(to), true);
			} else {
				return null;
			}	
		} else {
			return null;
		}
	}

	@Override
	public FilterBuilder createFilter(List<FilterCondition> conditions) {
		
		Map<String, List<FilterBuilder>> filterMap = new HashMap<String, List<FilterBuilder>>();
		for (FilterCondition condition: conditions) {
			String field = condition.getField();
			List<FilterBuilder> filters;
			if (filterMap.containsKey(field)) {
				filters = filterMap.get(field);
			} else {
				filters = new ArrayList<FilterBuilder>();
				filterMap.put(field, filters);
			}
			FilterBuilder filter = createFilter(condition);
			if (filter != null) {
				filters.add(filter);
			}
		}
		if (filterMap.isEmpty()) {
			return dateRangeFilter;
		} else if (filterMap.size() == 1) {
			List<FilterBuilder> filters = filterMap.values().iterator().next();
			FilterBuilder userFilter = createFieldFilterBuilder(filters);
			return FilterBuilders.and(dateRangeFilter, userFilter);
		} else {
			ArrayList<FilterBuilder> filters = new ArrayList<FilterBuilder>();
			for (Entry<String, List<FilterBuilder>> entry: filterMap.entrySet()) {
				filters.add(createFieldFilterBuilder(entry.getValue()));
			}
			FilterBuilder userFilter = FilterBuilders.and(filters);
			return FilterBuilders.and(dateRangeFilter, userFilter);
		}
	}
	
	private FilterBuilder createFieldFilterBuilder(List<FilterBuilder> filters) {
		if (filters.size() == 0) {
			return new EmptyFilterBuilder();
		} else if (filters.size() == 1) {
			return filters.get(0);
		} else {
			return FilterBuilders.or(filters);
		}
	}

	private long parseDateTime(String value) {
		try {
			return Long.parseLong(value);
		} catch (NumberFormatException e) {
			try {
				Date date = dateFormat.parse(value);
				return date.getTime();
			} catch (ParseException e1) {
				return -1;
			}
		}
	}
	
}

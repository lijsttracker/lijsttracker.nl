package nl.lijsttracker.search;

public class FilterCondition {

	private String field;
	private String condition;
	
	public FilterCondition(String field, String condition) {
		this.field = field;
		this.condition = condition;
	}
	
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return condition;
	}

	public void setValue(String condition) {
		this.condition = condition;
	}
	
}

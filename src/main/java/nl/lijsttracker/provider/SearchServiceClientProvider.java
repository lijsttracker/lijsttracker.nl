package nl.lijsttracker.provider;

import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.glassfish.hk2.api.Factory;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.ConfigurationManager;
import tv.mediadistillery.api.client.authenticationservice.AuthenticationServiceClient;
import tv.mediadistillery.api.client.platform.AuthorizedPlatformClient;
import tv.mediadistillery.api.client.platform.PlatformClient;
import tv.mediadistillery.api.client.platform.RetryPlatformClient;
import tv.mediadistillery.api.client.searchservice.HttpSearchServiceClient;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;

@Service
@Singleton
public class SearchServiceClientProvider implements Factory<SearchServiceClient> {

	private final AuthenticationServiceClient authServiceClient;
	private final ConfigurationManager config;
	private final AtomicReference<SearchServiceClient> clientInstance = new AtomicReference<>();
	
	@Inject
	public SearchServiceClientProvider(AuthenticationServiceClient authServiceClient, ConfigurationManager config) {
		this.authServiceClient = authServiceClient;
		this.config = config;
	}

	@Override
	public SearchServiceClient provide() {
		if (clientInstance.get() == null) {
			PlatformClient platformClient = new RetryPlatformClient(authServiceClient, new AuthorizedPlatformClient());
			String host = config.getProperty("searchservice.host");
			SearchServiceClient client = new HttpSearchServiceClient.Builder()
				.setHost(host)
				.setApiVersion(3)
				.usePlatformClient(platformClient)
				.build();
			clientInstance.set(client);
		} 
		return clientInstance.get();
	}
	
	@Override
	public void dispose(SearchServiceClient instance) { }
	
}

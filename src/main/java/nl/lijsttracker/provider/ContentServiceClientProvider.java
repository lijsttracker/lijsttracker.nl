package nl.lijsttracker.provider;

import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.glassfish.hk2.api.Factory;
import org.jvnet.hk2.annotations.Service;

import nl.lijsttracker.configuration.ConfigurationManager;
import tv.mediadistillery.api.client.authenticationservice.AuthenticationServiceClient;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient;
import tv.mediadistillery.api.client.contentservice.HttpContentServiceClient;
import tv.mediadistillery.api.client.platform.AuthorizedPlatformClient;
import tv.mediadistillery.api.client.platform.PlatformClient;
import tv.mediadistillery.api.client.platform.RetryPlatformClient;

@Service
@Singleton
public class ContentServiceClientProvider implements Factory<ContentServiceClient> {

	private final AuthenticationServiceClient authServiceClient;
	private final ConfigurationManager config;
	private final AtomicReference<ContentServiceClient> clientInstance = new AtomicReference<>();
	
	@Inject
	public ContentServiceClientProvider(AuthenticationServiceClient authServiceClient, ConfigurationManager config) {
		this.authServiceClient = authServiceClient;
		this.config = config;
	}

	@Override
	public ContentServiceClient provide() {
		if (clientInstance.get() == null) {
			PlatformClient platformClient = new RetryPlatformClient(authServiceClient, new AuthorizedPlatformClient());
			String host = config.getProperty("contentservice.host");
			int apiVersion = config.getIntProperty("contentservice.version", 1);
			ContentServiceClient client = new HttpContentServiceClient.Builder()
				.setHost(host)
				.usePlatformClient(platformClient)
				.setApiVersion(apiVersion)
				.build();
			clientInstance.set(client);
		}
		return clientInstance.get();
	}

	@Override
	public void dispose(ContentServiceClient client) {	}
	
}

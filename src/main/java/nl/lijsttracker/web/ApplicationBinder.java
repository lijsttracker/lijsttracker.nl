package nl.lijsttracker.web;

import javax.inject.Singleton;

import org.apache.commons.lang3.ArrayUtils;
import org.glassfish.hk2.utilities.BuilderHelper;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import tv.mediadistillery.api.client.authenticationservice.AuthenticationServiceClient;
import tv.mediadistillery.api.client.contentservice.ContentServiceClient;
import tv.mediadistillery.api.client.searchservice.SearchServiceClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.netflix.astyanax.Keyspace;

import nl.lijsttracker.configuration.LijsttrekkersConfiguration;
import nl.lijsttracker.configuration.ConfigurationManager;
import nl.lijsttracker.configuration.JsonLijsttrekkersConfiguration;
import nl.lijsttracker.configuration.YamlConfigurationManager;
import nl.lijsttracker.impact.AstyanaxImpactDAO;
import nl.lijsttracker.impact.ImpactDAO;
import nl.lijsttracker.impact.KeyspaceFactory;
import nl.lijsttracker.provider.AuthenticationServiceClientProvider;
import nl.lijsttracker.provider.ContentServiceClientProvider;
import nl.lijsttracker.provider.SearchServiceClientProvider;
import nl.lijsttracker.service.LijsttrekkerService;
import nl.lijsttracker.service.CachedLijsttrekkerService;
import nl.lijsttracker.service.ImpactService;
import nl.lijsttracker.service.ImpactServiceImpl;
import nl.lijsttracker.stats.StatsCollector;
import nl.lijsttracker.stats.StatsCollectorImpl;

public class ApplicationBinder extends AbstractBinder {
	
	private static final String[] CONFIG_FILES = ArrayUtils.toArray("/etc/lijsttracker/lijsttracker-config.yaml",
			"./src/main/resources/lijsttracker-config.yaml",
			"./lijsttracker-config.yaml",
			"lijsttracker-config.yaml");
	
	@Override
	protected void configure() {
		try {
			// Configuration
			ConfigurationManager config = new YamlConfigurationManager(CONFIG_FILES);
			bind(BuilderHelper.createConstantDescriptor(config, null, ConfigurationManager.class));
			
			// Jackson
			ObjectMapper jackson = new ObjectMapper();
			jackson.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
			bind(BuilderHelper.createConstantDescriptor(jackson, null, ObjectMapper.class));
            
			// Providers
			bindFactory(AuthenticationServiceClientProvider.class).to(AuthenticationServiceClient.class);
			bindFactory(SearchServiceClientProvider.class).to(SearchServiceClient.class);
			bindFactory(ContentServiceClientProvider.class).to(ContentServiceClient.class);
			
            // Bind services
			bind(StatsCollectorImpl.class).to(StatsCollector.class);
			bindAsContract(CachedLijsttrekkerService.class).to(LijsttrekkerService.class).in(Singleton.class);
			bindAsContract(ImpactServiceImpl.class).to(ImpactService.class).in(Singleton.class);
			//bindAsContract(AthleteClipServiceImpl.class).to(AthleteClipService.class).in(Singleton.class);
			bind(JsonLijsttrekkersConfiguration.class).to(LijsttrekkersConfiguration.class);
			
			// DAO
			bind(AstyanaxImpactDAO.class).to(ImpactDAO.class);
			bindFactory(KeyspaceFactory.class).to(Keyspace.class).in(Singleton.class);
			
		} catch (Exception e) {
			throw new RuntimeException("Unable to initialize application", e);
		}
		
	}

}
